import * as Minio from "minio";

export default class S3Service {
  private readonly host: string
  private readonly bucket: string
  private readonly prefix: string
  private readonly accessKey: string
  private readonly secretKey: string

  constructor(env: any) {
    this.host = env.S3_HOST
    this.bucket = env.S3_BUCKET
    this.prefix = env.S3_PREFIX
    this.accessKey = env.S3_ACCESS_KEY
    this.secretKey = env.S3_SECRET_KEY
  }

  public client() {
    return new Minio.Client({
      endPoint: this.host,
      useSSL: true,
      accessKey: this.accessKey,
      secretKey: this.secretKey
    })
  }

  public getBucket(): string {
    return this.bucket
  }

  public getPrefix(): string {
    return this.prefix
  }
}