import {NextApiRequest, NextApiResponse} from "next";
import S3Service from "services/s3";
import {BucketStream} from "minio";

export default async function api(req: NextApiRequest, res: NextApiResponse) {
  if (!req.query.folderId) {
    return res.status(401).json({ success: false, error: 'Unauthorized access.' })
  }

  const s3Service: S3Service = new S3Service(process.env)
  const prefix: string = `${s3Service.getPrefix()}/${req.query.folderId}`
  const stream: BucketStream<any> = s3Service.client().listObjectsV2(s3Service.getBucket(), prefix, true)

  await new Promise((resolve) => {
    let error: any = undefined
    let files: Array<any> = []

    stream.on('data', (data: any) => {
      if (data.name && data.name.indexOf('.init') < 0 && data.size != 0) {
        files.push({
          ...data,
          filename: data.name.split('/')[data.name.split('/').length-1],
          url: `${process.env.S3_BUCKET_URL}/${data.name}`
        })
      }
    })
    stream.on('error', (e: any) => error = e)
    stream.on('end', () => {
      if (error) res.status(500).json({ success: false, error: error })
      else res.status(200).json({ success: true, data: files})
      resolve(() => {})
    })
  })
}