import Fetch from "util/fetch";
import {Button, notification, Table, Upload} from "antd";
import {NextPageContext} from "next";
import moment from "moment";
import {useState} from "react";
import Logo from "../components/icon/logo";

export default function Home({ baseUrl, folder, success, data }: any) {
  const [files, setFiles] = useState<Array<any>>(data)

  const props = {
    name: 'file',
    multiple: false,
    action: '/api/v1/s3/upload',
    data: {
      folderId: folder
    },
    onChange: async (info: any) => {
      const { status } = info.file;
      if (status === 'done') {
        notification.success({ message: 'Success', description: `${info.file.name} file uploaded successfully.` });
        try {
          const result: any = await Fetch('GET', `${baseUrl}/api/v1/s3/list-folder?folderId=${folder}`)
          if (result.data) setFiles(result.data)
        } catch (e) {
          notification.error({ message: 'Error', description: `Failed to reload newest files.` });
        }
      } else if (status === 'error') {
        notification.error({ message: 'Error', description: info.file.response.error });
      }
    },
    onDrop: (e: any) => {
      console.log('Dropped files', e.dataTransfer.files);
    },
  };

  return (
    <div className={`container mx-auto py-5`}>
      <a href={`/`}><Logo /></a>
      <div className={`my-5`}>
        <Upload.Dragger {...props}>
          <p className="ant-upload-drag-icon">
            <i className={`fa fa-cloud-upload fa-2x`} />
          </p>
          <p className="ant-upload-text">Click or drag file to this area to upload</p>
        </Upload.Dragger>
      </div>
      <Table columns={[
        { key: 'filename', dataIndex: 'filename', title: 'Name' },
        { key: 'lastModified', dataIndex: 'lastModified', title: 'Last Modified', render: (lastModified) => moment(lastModified).format(`DD-MM-YYYY HH:mm:ss`) },
        {
          key: 'url', dataIndex: 'url', title: 'Action',
          render: (url: string) => (
            <Button type={`primary`} onClick={e => {
              navigator.clipboard.writeText(url)
              notification.success({ message: 'Info', description: 'URL has been copied.', duration: 2 })
            }}>
              Copy URL
            </Button>
          )
        },
      ]} dataSource={files} rowKey={`filename`} />
    </div>
  )
}

export async function getServerSideProps(ctx: NextPageContext) {
  const {query} = ctx
  const result: any = await Fetch('GET', `${process.env.BASE_URL}/api/v1/s3/list-folder?folderId=${query.folder}`)
  return {
    props: {
      baseUrl: process.env.BASE_URL,
      ...result,
      ...query
    }
  }
}